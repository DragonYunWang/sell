import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
// 引入全局样式
import 'common/stylus/index.styl'

Vue.config.productionTip = false

// 将axios挂载到prototype上，在组件中可以直接使用this.axios访问
Vue.prototype.axios = axios

/* eslint-disable no-new */
new Vue({
  el: '#app',
  axios,
  router,
  render: h => h(App)
})
