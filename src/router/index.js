import Vue from 'vue'
import Router from 'vue-router'

const Goods = () => import('components/goods/goods')
const Seller = () => import('components/seller/seller')
const Ratings = () => import('components/ratings/ratings')

Vue.use(Router)

const routes = [
  {
    path: '/',
    name: 'Goods',
    component: Goods
  },
  {
    path: '/goods',
    name: 'Goods',
    component: Goods
  },
  {
    path: '/seller',
    name: 'Seller',
    component: Seller
  },
  {
    path: '/ratings',
    name: 'Ratings',
    component: Ratings
  }
]

export default new Router({
  // mode: 'history',
  routes,
  linkActiveClass: 'active'
})
